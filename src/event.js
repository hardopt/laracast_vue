import Vue from 'vue'
window.Event = new class {
    constructor() {
        this.vue = new Vue;
    }

    fire(event, data = false) {
        this.vue.$emit(event, data)
    }

    listen(event, callback) {
        this.vue.$on(event, callback)
    }
}
